## Artwork and stuff

As the name suggests, these are for "art" I created. These include wallpapers and others. 

Since many of the source materials I used are licensed under CC BY-SA 2.5, you can assume everything in this repo to be under CC BY-SA 2.5. 
